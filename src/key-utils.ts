export class KeySequence {
  sequence: KeyEvent[];
}

// Modifiers show up as the key code if no other key is pressed
const extraKeys = [
  "AltLeft",
  "ControlLeft",
  "ControlRight",
  "MetaLeft",
  "MetaRight",
  "OSLeft",
  "OSRight",
  "ShiftLeft",
  "ShiftRight",
  "F5",
];

// Map between names and props
const modifiers = [
  ["ctrlKey", "Ctrl"],
  ["shiftKey", "Shift"],
  ["metaKey", "Meta"],
  ["altKey", "Alt"],
];

interface KeyEventObject {
  key: string;
  altKey?: boolean;
  ctrlKey?: boolean;
  shiftKey?: boolean;
  metaKey?: boolean;
  originalEvent?: KeyboardEvent;
}

export class KeyEvent {
  key: string;
  altKey = false;
  ctrlKey = false;
  shiftKey = false;
  metaKey = false;
  originalEvent?: KeyboardEvent;

  static fromObject(keyEventObject: KeyEventObject): KeyEvent {
    const keyEvent = new KeyEvent();
    keyEvent.key = keyEventObject.key;
    keyEvent.altKey = !!keyEventObject.altKey;
    keyEvent.ctrlKey = !!keyEventObject.ctrlKey;
    keyEvent.shiftKey = !!keyEventObject.shiftKey;
    keyEvent.metaKey = !!keyEventObject.metaKey;
    keyEvent.originalEvent = keyEventObject.originalEvent || null;
    return keyEvent;
  }

  // Create a new KeyEvent from a source event
  static fromEvent(event: JQuery.KeyboardEventBase | KeyboardEvent): KeyEvent {
    if (isJQueryEvent(event)) {
      event = event.originalEvent;
    }
    if (extraKeys.includes(event.code)) {
      return null;
    }

    const keyEvent = new KeyEvent();
    keyEvent.originalEvent = event;
    keyEvent.altKey = event.altKey;
    keyEvent.ctrlKey = event.ctrlKey;
    keyEvent.shiftKey = event.shiftKey;
    keyEvent.metaKey = event.metaKey;
    keyEvent.key = event.code;

    return keyEvent;
  }

  static fromString(str: string): KeyEvent {
    const pieces = str.split("+").map(key => key.trim());
    const keyEvent = new KeyEvent();

    for (const piece of pieces) {
      const mod = modifiers.find(([, name]) => name === piece);
      if (mod) {
        keyEvent[mod[0]] = true;
      } else if (keyEvent.key) {
        throw new Error(
          "More than one non-modifier detected in key combination"
        );
      } else {
        keyEvent.key = piece;
      }
    }

    return keyEvent;
  }

  toString(): string {
    const keys = modifiers.filter(([mod]) => this[mod]).map(([, name]) => name);
    if (this.key) {
      keys.push(this.key);
    }
    return keys.join(" + ");
  }

  static getEvent(event: AnyEventFormat): KeyEvent {
    if (event instanceof KeyEvent) return event;

    if (
      event?.constructor?.name == "KeyboardEvent" ||
      event["originalEvent"] ||
      event["code"]
    ) {
      return KeyEvent.fromEvent(event as KeyboardEvent);
    }
    if (typeof event === "string") {
      return KeyEvent.fromString(event);
    }
    console.log(event);
  }

  equals(keyEvent: KeyEvent): boolean {
    if (!keyEvent) return false;
    return (
      this.altKey == keyEvent.altKey &&
      this.ctrlKey == keyEvent.ctrlKey &&
      this.shiftKey == keyEvent.shiftKey &&
      this.metaKey == keyEvent.metaKey &&
      this.key == keyEvent.key
    );
  }
}
// TODO: Support KeyEventObject interface?
export type AnyEventFormat =
  | KeyEvent
  | string
  | JQuery.KeyboardEventBase
  | KeyboardEvent;

export function matchEvent(
  event1: AnyEventFormat,
  event2: AnyEventFormat
): boolean {
  const keyEvent1 = KeyEvent.getEvent(event1);
  const keyEvent2 = KeyEvent.getEvent(event2);
  return keyEvent1?.equals(keyEvent2);
}

// Type utils
export function isJQueryEvent(
  event: JQuery.KeyboardEventBase | KeyboardEvent
): event is JQuery.KeyboardEventBase {
  return !!event["originalEvent"];
}
