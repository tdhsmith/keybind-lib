import { KeybindConfigurator } from "./configurator";
import { KeyEvent, AnyEventFormat, matchEvent } from "./key-utils";

interface RegistrationOptions {
  // "name" exists only to allow interface same as game.settings.register
  name?: string;
  title?: string;
  hint?: string;
  default?: AnyEventFormat;
  config?: boolean;
  bindConfig?: boolean;
  onKeyDown?: (evt: KeyEvent) => void;
  onKeyUp?: (evt: KeyEvent) => void;
  scope?: "client" | "world";
  type?;
}

class RegisteredKeybind {
  moduleName: string;
  bindName: string;

  options: RegistrationOptions;

  public get keyBind(): KeyEvent {
    const setting = game.settings.get(this.moduleName, this.bindName);
    if (setting) {
      return KeyEvent.fromString(setting);
    }
  }

  constructor(moduleName, bindName, options: RegistrationOptions) {
    this.moduleName = moduleName;
    this.bindName = bindName;
    this.options = { ...options, title: options.title || options.name };
    delete this.options.name;
    // Load keybind from settings
  }
}

const defaultOptions: RegistrationOptions = {
  scope: "client",
  config: false,
  bindConfig: true,
  type: String,
};

class KeybindLib {
  static registeredBinds = new Map<string, RegisteredKeybind>();

  static isBoundTo(
    evt: AnyEventFormat,
    moduleName: string,
    bindName: string
  ): boolean {
    const bind = this.registeredBinds.get(`${moduleName}.${bindName}`);
    if (!bind) return false;
    return matchEvent(evt, bind.keyBind);
  }

  static register(
    moduleName: string,
    bindName: string,
    options?: RegistrationOptions
  ): void {
    if (!game.modules.get(moduleName)) {
      throw new Error(`Module does not exist: ${moduleName}`);
    }
    const fullName = `${moduleName}.${bindName}`;
    const opt = options
      ? { ...defaultOptions, ...options, fullName }
      : defaultOptions;
    if (this.registeredBinds.has(fullName))
      throw new Error(
        `Keybind already registered, module: ${moduleName}, bindName: ${bindName}`
      );

    const setting = game.settings.settings.get(`${moduleName}.${bindName}`);
    if (setting) {
      if (setting.type != String) {
        throw new Error(
          `Keybind Lib | Setting for keybind already registered, needs to be String. Incorrect type: ${setting.type}, module: ${moduleName}, bindName: ${bindName}`
        );
      }
      console.info(
        `Keybind Lib | Setting for keybind already registered, will be used; module: ${moduleName}, bindName: ${bindName}`
      );
    } else {
      game.settings.register(moduleName, bindName, {
        ...options,
        type: String,
      });
    }

    this.registeredBinds.set(
      fullName,
      new RegisteredKeybind(moduleName, bindName, opt)
    );
  }

  private static _onKeyDown(evt: KeyboardEvent): void {
    const keyEvent = KeyEvent.fromEvent(evt);
    if (!keyEvent) return;
    for (const [bindName, bind] of this.registeredBinds.entries()) {
      if (bind.keyBind.equals(keyEvent)) {
        console.log("Keybind Lib | Key down", bindName);
        bind.options?.onKeyDown?.(keyEvent);
      }
    }
  }

  private static _onKeyUp(evt: KeyboardEvent): void {
    const keyEvent = KeyEvent.fromEvent(evt);
    if (!keyEvent) return;
    for (const [bindName, bind] of this.registeredBinds.entries()) {
      if (bind.keyBind.equals(keyEvent)) {
        console.log("Keybind Lib | Key up", bindName);
        bind.options?.onKeyUp?.(keyEvent);
      }
    }
  }

  static init(): void {
    document.addEventListener("keydown", this._onKeyDown.bind(KeybindLib));
    document.addEventListener("keyup", this._onKeyUp.bind(KeybindLib));
  }
}

Hooks.on("ready", () => {
  KeybindLib.init();
  game.settings.registerMenu("keybind-lib", "keybinds", {
    name: "Custom module keybinds",
    label: "Edit keybinds",
    icon: "fas fa-keyboard",
    type: KeybindConfigurator,
  });
});

Hooks.on("renderSettingsConfig", (app, html: JQuery<HTMLElement>) => {
  for (const [fullName] of KeybindLib.registeredBinds.entries()) {
    const input = $(html).find(
      `input[name="${fullName}"]`
    ) as JQuery<HTMLInputElement>;
    input.attr("readonly", "true");
    input
      .parent()
      .append(`<i class="far fa-keyboard keybind-lib-settings-icon"></i>`);
    input.on("keydown", evt => {
      const keyEvent = KeyEvent.fromEvent(evt);
      if (!keyEvent) return;
      evt.preventDefault();
      evt.stopPropagation();
      evt.target.value = keyEvent.toString();
    });
  }
});

globalThis.KeybindLib = KeybindLib;
export { KeybindLib };
