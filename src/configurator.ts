import { KeyEvent } from "./key-utils";

export class KeybindConfigurator extends FormApplication {
  _state: number;

  constructor() {
    super(null);
  }

  get element(): JQuery<HTMLElement> {
    return super.element as JQuery<HTMLElement>;
  }

  static get defaultOptions(): FormApplicationOptions {
    return {
      ...super.defaultOptions,
      title: "Keybinds",
      id: "keybind-configurator",
      template: "modules/keybind-lib/templates/configurator.hbs",
      resizable: true,
      width: 500,
      closeOnSubmit: false,
      submitOnChange: false,
      submitOnClose: false,
    };
  }

  getData(): any {
    const data = {
      modules: [],
    };
    for (const [
      name,
      registeredBind,
    ] of globalThis.KeybindLib.registeredBinds.entries()) {
      const [moduleName, bindName] = name.split(".");
      let moduleData = data.modules.find(m => m.name == moduleName);
      if (!moduleData) {
        moduleData = {
          name: moduleName,
          title: game.modules.get(moduleName).data.title,
          binds: [],
        };
        data.modules.push(moduleData);
      }

      moduleData.binds.push({
        bindName,
        ...registeredBind.options,
        key: registeredBind.keyBind.toString(),
      });
    }

    return data;
  }

  resetSetting(fullName: string): string {
    const [moduleName, bindName] = fullName.split(".");
    const defaultSetting = game.settings.settings.get(fullName).default;
    if (defaultSetting) {
      game.settings.set(moduleName, bindName, defaultSetting);
    }
    return defaultSetting;
  }

  activateListeners(html): void {
    super.activateListeners(html);
    this.element.find("input").on("keydown", evt => {
      const keyEvent = KeyEvent.fromEvent(evt);
      if (!keyEvent) return;
      evt.preventDefault();
      evt.stopPropagation();
      evt.target.value = keyEvent.toString();
    });

    this.element.find(`button[name="reset"]`).on("click", () => {
      //@ts-ignore
      const formData = this._getSubmitData();

      for (const fullName of Object.keys(formData)) {
        this.resetSetting(fullName);
      }
      this.render(true);
    });

    this.element.find(".fa-undo[data-bind]").on("click", evt => {
      const fullName = evt.target.dataset["bind"];
      if (!fullName) return;
      const val = this.resetSetting(fullName);
      this.element.find(`input[name="${fullName}"]`).val(val);
    });
  }

  async _updateObject(event, formData): Promise<void> {
    for (const [fullName, value] of Object.entries(formData)) {
      const [moduleName, bindName] = fullName.split(".");
      game.settings.set(moduleName, bindName, value);
    }
    this.render(true);
  }
}
