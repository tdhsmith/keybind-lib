## [0.2.0] - 2021-01-20

Breaking change!

### Changed

- Replaced `callback` function with `onKeyDown` and `onKeyUp`

## [0.1.0] - 2021-01-20

First release
