# Keybind Lib

Keybind Lib is a library and configurator for custom keybinds. Keybinds can be
registered by modules and edited by users in the configurator.

# Usage - API

This is the basic API for registering and using keybinds. It closely models the
`game.settings.register` API.

## Register keybinds

When registering a keybind, it will also automatically register a setting under
your module with the "bindName". You can set `config: true` and the setting will
show up in the regular module settings flow too!

In fact, the entire options object will be passed on to
`game.settings.register`.

```JavaScript
Hooks.on("init", () => {
  // Binding with a default key and a simple callback
  KeybindLib.register("moduleName", "bindName", {
    name: "Open Quick Insert",
    hint: "Opens Quick Insert anywhere in Foundry",
    default: "Ctrl + Space",
    onKeyDown: () => {
      console.log("Key pressed!");
    }
  });
})
```

## Manual matching

If you don't supply a callback function, you can still parse events yourself,
this can be useful if you want to set up your own event listeners.

```JavaScript
$("#my-element").on('keydown', (event) => {
	if (KeybindLib.isBoundTo(event, "moduleName", "bindName")) {
    console.log("Key was pressed on my element");
	}
});
```

# Contributing

Keybind Lib is a library built for the Foundry VTT community by the Foundry VTT
community. Merge request are very welcome, but for large/complex changes, please
open an issue to discuss the matter first. Any API changes have to be backwards
compatible.

## Merge request process

1. Base your merge request on the develop branch and it should automatically be
   built and validated
